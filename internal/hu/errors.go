package hu

import (
	"errors"
	"net/http"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cafemenu/cafemenu/internal/errreason"
	"gitlab.com/cafemenu/cafemenu/internal/logger"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/models"
	"go.uber.org/zap"
)

type errorResponder struct {
	err         error
	contentType string
}

func ErrorResponder(r *http.Request, err error) middleware.Responder {
	return &errorResponder{
		err:         err,
		contentType: "application/json",
	}
}

func (e *errorResponder) WriteResponse(w http.ResponseWriter, p runtime.Producer) {
	var werr *errreason.Error
	userMsg := errreason.DefaultUserMsg
	code := http.StatusInternalServerError
	if errors.As(e.err, &werr) {
		userMsg = werr.UserMsg()
		code = werr.Code()
	}

	w.Header().Set("content-type", e.contentType)
	w.WriteHeader(code)
	m := models.BaseError{
		Message:         userMsg,
		InternalMessage: e.err.Error(),
	}

	if err := p.Produce(w, m); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}

func WriteError(w http.ResponseWriter, r *http.Request, err error) {
	logger := logger.C(r.Context()).Desugar()
	defer func() {
		rec := recover()
		if rec != nil {
			logger.Error("writing error", zap.Any("recover", rec))
		}
	}()

	w.Header().Set("X-Content-Type-Options", "nosniff")
	ErrorResponder(r, err).WriteResponse(w, runtime.JSONProducer())
}
