package errreason

import (
	"errors"
	"fmt"
	"net/http"

	. "gitlab.com/cafemenu/cafemenu/internal/localization"
)

type Reason int

const (
	Undefined Reason = iota
	BadRequest
	NotFound
	Unauthorized
	Forbidden
	Conflict
)

var DefaultUserMsg = L("internal error")

var httpStatusMap = map[Reason]int{
	BadRequest:   http.StatusBadRequest,
	NotFound:     http.StatusNotFound,
	Unauthorized: http.StatusUnauthorized,
	Forbidden:    http.StatusForbidden,
	Conflict:     http.StatusConflict,
}

type Error struct {
	reason  Reason
	cause   error
	msg     string
	userMsg string
	*stack
}

func (err *Error) Error() string {
	return err.msg
}

func (err *Error) Code() int {
	if s, ok := httpStatusMap[err.reason]; ok {
		return s
	}

	return http.StatusInternalServerError
}

func (err *Error) UserMsg() string {
	if err.userMsg == "" {
		return DefaultUserMsg
	}

	return err.userMsg
}

func WithReason(err error, reason Reason) *Error {
	var werr *Error
	if errors.As(err, &werr) {
		werr.reason = reason
		return werr
	}

	return &Error{
		cause:  err,
		stack:  callers(),
		reason: reason,
	}
}

func Errorf(msg string, args ...interface{}) *Error {
	return &Error{
		msg:   fmt.Sprintf(msg, args...),
		stack: callers(),
	}
}

func Wrap(err error, message string) *Error {
	if err == nil {
		return nil
	}

	return &Error{
		cause: err,
		msg:   message + ":" + err.Error(),
		stack: callers(),
	}
}

func Wrapf(err error, message string, args ...interface{}) *Error {
	if err == nil {
		return nil
	}

	return &Error{
		cause: err,
		msg:   fmt.Sprintf(message, args...) + ":" + err.Error(),
		stack: callers(),
	}
}

func WithStack(err error) *Error {
	var werr *Error
	if errors.As(err, &werr) {
		werr.stack = callers()
		return werr
	}

	return &Error{
		cause: err,
		stack: callers(),
	}
}

func (err *Error) WithMsg(msg string) *Error {
	err.userMsg = msg
	return err
}

func (err *Error) WithMsgf(msg string, args ...interface{}) *Error {
	err.userMsg = fmt.Sprintf(msg, args...)
	return err
}

func UserErrorf(msg string, args ...interface{}) *Error {
	formatted := fmt.Sprintf(msg, args...)
	return &Error{
		msg:     formatted,
		userMsg: formatted,
		stack:   callers(),
	}
}

func (err *Error) WithReason(reason Reason) *Error {
	err.reason = reason
	return err
}
