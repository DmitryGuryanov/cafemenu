package logger

import (
	"context"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var globalSugaredLogger *zap.SugaredLogger

func Init() error {
	globalLoggerConfig := zap.NewProductionConfig()
	globalLoggerConfig.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	globalLoggerConfig.Level.SetLevel(zap.DebugLevel)
	globalLoggerConfig.Sampling = nil

	logger, err := globalLoggerConfig.Build()
	if err != nil {
		return err
	}

	globalSugaredLogger = logger.Sugar()
	return nil
}

type key int

const (
	loggerKey key = iota
)

// WithLogger attaches given logger to the context.
func WithLogger(ctx context.Context, logger *zap.SugaredLogger) context.Context {
	return context.WithValue(ctx, loggerKey, logger)
}

// L returns global loggers, the same as in zap package
func L() *zap.SugaredLogger {
	return globalSugaredLogger
}

// C returns logger, taken from context.
func C(ctx context.Context) *zap.SugaredLogger {
	logger, ok := ctx.Value(loggerKey).(*zap.SugaredLogger)
	if ok {
		return logger
	}

	if globalSugaredLogger != nil {
		return globalSugaredLogger
	}

	return zap.L().Sugar()
}
