package notify

import (
	"bytes"
	"context"
	"fmt"
	"text/template"

	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/logger"
)

type Template int

const (
	TemplateRegister = iota
)

var templates = map[Template]*template.Template{
	TemplateRegister: template.Must(template.New("register").Parse(`Enter code {{.code}} to finish registration`)),
}

type Notifier struct {
	db *db.DB
}

func New(dbh *db.DB) *Notifier {
	return &Notifier{
		db: dbh,
	}
}

func (n *Notifier) Send(ctx context.Context, email string, tmpl Template, args map[string]interface{}) error {
	t, ok := templates[tmpl]
	if !ok {
		return fmt.Errorf("template %d is not found", tmpl)
	}

	var txt bytes.Buffer
	if err := t.Execute(&txt, args); err != nil {
		return err
	}

	logger.C(ctx).Infow("Show notification",
		"email", email,
		"text", txt)

	return nil
}
