package db

import (
	"fmt"
	"reflect"
)

func GetFields(s interface{}) (ret []string) {
	t := reflect.TypeOf(s)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	if t.Kind() != reflect.Struct {
		panic(fmt.Sprintf("invalid type passed to GetFields: %T", s))
	}

	for i := 0; i < t.NumField(); i++ {
		ft := t.Field(i)
		tag := ft.Tag.Get("db")
		if tag == "" {
			continue
		}

		ret = append(ret, tag)
	}

	return ret
}
