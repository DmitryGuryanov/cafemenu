package db

import (
	"context"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"

	"gitlab.com/cafemenu/cafemenu/internal/logger"
)

type DB struct {
	DB       sqlx.ExtContext
	txRunner TxRunner
}

type pgxLogger struct{}

func (pl *pgxLogger) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	fields := make([]interface{}, 0, 2*len(data))
	for k, v := range data {
		fields = append(fields, k, v)
	}
	logger.C(ctx).Debugw(msg, fields...)
}

func Init(connStr string) (*DB, error) {
	cfg, err := pgx.ParseConfig(connStr)
	if err != nil {
		return nil, err
	}

	cfg.Logger = &pgxLogger{}
	loggedConnStr := stdlib.RegisterConnConfig(cfg)

	db, err := sqlx.Connect("pgx", loggedConnStr)
	if err != nil {
		return nil, err
	}

	return &DB{
		DB:       db,
		txRunner: db,
	}, nil
}
