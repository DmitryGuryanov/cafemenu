package db

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"go.uber.org/multierr"
)

func sqlErr(err error, query string, args ...interface{}) error {
	return fmt.Errorf(`run query "%s" with args %+v: %w`, query, args, err)
}

func namedQuery(query string, arg interface{}) (nq string, args []interface{}, err error) {
	nq, args, err = sqlx.Named(query, arg)
	if err != nil {
		return "", nil, sqlErr(err, query, args...)
	}
	return nq, args, nil
}

func (db *DB) Exec(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	res, err := db.DB.ExecContext(ctx, query, args...)
	if err != nil {
		return res, sqlErr(err, query, args...)
	}

	return res, nil
}

func (db *DB) NamedExec(ctx context.Context, query string, arg interface{}) (sql.Result, error) {
	nq, args, err := namedQuery(query, arg)
	if err != nil {
		return nil, err
	}

	return db.Exec(ctx, db.DB.Rebind(nq), args...)
}

func (db *DB) Select(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	if err := sqlx.SelectContext(ctx, db.DB, dest, query, args...); err != nil {
		return sqlErr(err, query, args...)
	}

	return nil
}

func (db *DB) NamedSelect(ctx context.Context, dest interface{}, query string, arg interface{}) error {
	nq, args, err := namedQuery(query, arg)
	if err != nil {
		return err
	}

	return db.Select(ctx, dest, db.DB.Rebind(nq), args...)
}

func (db *DB) Get(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	if err := sqlx.GetContext(ctx, db.DB, dest, query, args...); err != nil {
		return sqlErr(err, query, args...)
	}

	return nil
}

func (db *DB) NamedGet(ctx context.Context, dest interface{}, query string, arg interface{}) error {
	nq, args, err := namedQuery(query, arg)
	if err != nil {
		return err
	}

	return db.Get(ctx, dest, db.DB.Rebind(nq), args...)
}

func (db *DB) SelectMaps(ctx context.Context, query string, args ...interface{}) (ret []map[string]interface{}, err error) {
	rows, err := db.DB.QueryxContext(ctx, query, args...)
	if err != nil {
		return nil, sqlErr(err, query, args...)
	}

	defer func() {
		err = multierr.Combine(err, rows.Close())
	}()

	ret = []map[string]interface{}{}
	numCols := -1
	for rows.Next() {
		var m map[string]interface{}
		if numCols < 0 {
			m = map[string]interface{}{}
		} else {
			m = make(map[string]interface{}, numCols)
		}

		if err = rows.MapScan(m); err != nil {
			return nil, sqlErr(err, query, args...)
		}
		ret = append(ret, m)
		numCols = len(m)
	}

	if err = rows.Err(); err != nil {
		return nil, sqlErr(err, query, args...)
	}

	return ret, nil
}

func (db *DB) NamedSelectMaps(ctx context.Context, query string, arg interface{}) (ret []map[string]interface{}, err error) {
	nq, args, err := namedQuery(query, arg)
	if err != nil {
		return nil, err
	}

	return db.SelectMaps(ctx, db.DB.Rebind(nq), args...)
}

func (db *DB) GetMap(ctx context.Context, query string, args ...interface{}) (ret map[string]interface{}, err error) {
	row := db.DB.QueryRowxContext(ctx, query, args...)
	if row.Err() != nil {
		return nil, sqlErr(row.Err(), query, args...)
	}

	ret = map[string]interface{}{}
	if err := row.MapScan(ret); err != nil {
		return nil, sqlErr(err, query, args...)
	}

	return ret, nil
}

func (db *DB) NamedGetMap(ctx context.Context, query string, arg interface{}) (ret map[string]interface{}, err error) {
	nq, args, err := namedQuery(query, arg)
	if err != nil {
		return nil, err
	}

	return db.GetMap(ctx, db.DB.Rebind(nq), args...)
}

type TxFunc func(tx *DB) error

type TxRunner interface {
	BeginTxx(context.Context, *sql.TxOptions) (*sqlx.Tx, error)
}

func (db *DB) RunTx(ctx context.Context, f TxFunc) (err error) {
	var tx *sqlx.Tx

	if db.txRunner == nil {
		return fmt.Errorf("nested transactions are not supported")
	}

	opts := &sql.TxOptions{
		Isolation: sql.LevelReadCommitted,
	}

	tx, err = db.txRunner.BeginTxx(ctx, opts)
	if err != nil {
		return fmt.Errorf("begin transaction: %w", err)
	}
	defer func() {
		if err != nil {
			err = multierr.Combine(err, tx.Rollback())
		} else {
			err = tx.Commit()
		}
	}()

	return f(&DB{
		DB: tx,
	},
	)
}
