package admincafe

import (
	"context"
	"fmt"

	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/errreason"
	"gitlab.com/cafemenu/cafemenu/internal/hu"
	. "gitlab.com/cafemenu/cafemenu/internal/localization"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/models"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops/admin"
)

func validateUpdateReq(req *models.AdminCafeUpdateReq) string {
	if !validatePhone(req.Phone) {
		return L("phone is not valid")
	}

	return ""
}

func HandleUpdateCafe(ctx context.Context, dbh *db.DB, p admin.UpdateCafeParams, u interface{}) middleware.Responder {
	if msg := validateUpdateReq(p.Req); msg != "" {
		return hu.ErrorResponder(p.HTTPRequest, errreason.UserErrorf(msg).WithReason(errreason.BadRequest))
	}

	cf, err := updateCafe(ctx, dbh, p.CafeID, p.Req)
	if err != nil {
		return hu.ErrorResponder(p.HTTPRequest, err)
	}

	return admin.NewUpdateCafeOK().WithPayload(cf.toAPI())
}

func updateCafe(ctx context.Context, dbh *db.DB, id int64, req *models.AdminCafeUpdateReq) (cf *cafe, err error) {
	err = dbh.RunTx(ctx, func(tx *db.DB) error {
		cf, err = updateCafeTx(ctx, tx, id, req)
		return err
	})

	return cf, err
}

func updateCafeTx(ctx context.Context, tx *db.DB, id int64, req *models.AdminCafeUpdateReq) (*cafe, error) {
	q := `select exists (select 1 from cafes where id = $1) x`
	var exists bool
	if err := tx.Get(ctx, &exists, q, id); err != nil {
		return nil, err
	}

	if !exists {
		return nil, errreason.UserErrorf(L("cafe with id %d doesn't exists"), id).WithReason(errreason.NotFound)
	}

	q = `select exists (select 1 from cafes where name = $1) x`
	if err := tx.Get(ctx, &exists, q, req.Name); err != nil {
		return nil, err
	}

	if exists {
		return nil, errreason.UserErrorf(L("cafe with name '%s' already exists"), req.Name).WithReason(errreason.Conflict)
	}

	q = fmt.Sprintf(`update cafes set
			name = $1, address = $2, phone = $3, inn = $4, updated_at = now()
		where id = $5
		returning %s`, cafeFields)
	var cf cafe
	if err := tx.Get(ctx, &cf, q, req.Name, req.Phone, req.Address, req.INN, id); err != nil {
		return nil, err
	}

	return &cf, nil
}
