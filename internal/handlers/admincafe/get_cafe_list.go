package admincafe

import (
	"context"
	"fmt"

	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/hu"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/models"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops/admin"
)

func HandleGetCafeList(ctx context.Context, dbh *db.DB, p admin.GetCafeListParams, u interface{}) middleware.Responder {
	var cfs []cafe
	q := fmt.Sprintf(`select %s from cafes`, cafeFields)
	if err := dbh.Select(ctx, &cfs, q); err != nil {
		return hu.ErrorResponder(p.HTTPRequest, err)
	}

	ret := &models.AdminCafeList{
		Items: make([]*models.AdminCafe, 0, len(cfs)),
		Total: int64(len(cfs)),
	}
	for _, cf := range cfs {
		ret.Items = append(ret.Items, cf.toAPI())
	}

	return admin.NewGetCafeListOK().WithPayload(ret)
}
