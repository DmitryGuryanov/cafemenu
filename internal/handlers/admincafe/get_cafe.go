package admincafe

import (
	"context"
	"fmt"

	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/hu"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops/admin"
)

func HandleGetCafe(ctx context.Context, dbh *db.DB, p admin.GetCafeParams, u interface{}) middleware.Responder {
	var cf cafe
	q := fmt.Sprintf(`select %s from cafes where id = $1`, cafeFields)
	if err := dbh.Get(ctx, &cf, q, p.CafeID); err != nil {
		return hu.ErrorResponder(p.HTTPRequest, err)
	}

	return admin.NewGetCafeOK().WithPayload(cf.toAPI())
}
