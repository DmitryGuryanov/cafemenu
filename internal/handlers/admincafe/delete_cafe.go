package admincafe

import (
	"context"

	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/hu"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops/admin"
)

func HandleDeleteCafe(ctx context.Context, dbh *db.DB, p admin.DeleteCafeParams, u interface{}) middleware.Responder {
	q := `update cafes set deleted_at = now() where id = $1`
	if _, err := dbh.Exec(ctx, q, p.CafeID); err != nil {
		return hu.ErrorResponder(p.HTTPRequest, err)
	}

	return admin.NewDeleteCafeOK()
}
