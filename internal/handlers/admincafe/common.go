package admincafe

import (
	"database/sql"
	"regexp"
	"strings"
	"time"

	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/models"
)

type cafe struct {
	ID        int64        `db:"id"`
	Name      string       `db:"name"`
	Address   string       `db:"address"`
	Phone     string       `db:"phone"`
	INN       string       `db:"inn"`
	CreatedAt time.Time    `db:"created_at"`
	UpdatedAt sql.NullTime `db:"updated_at"`
	DeletedAt sql.NullTime `db:"deleted_at"`
}

var cafeFieldsList = db.GetFields(cafe{})
var cafeFields = strings.Join(cafeFieldsList, ", ")

func nullTimeUnix(in sql.NullTime) int64 {
	if !in.Valid {
		return 0
	}

	return in.Time.Unix()
}

func (cf *cafe) toAPI() *models.AdminCafe {
	return &models.AdminCafe{
		ID:        cf.ID,
		Name:      cf.Name,
		Address:   cf.Address,
		Phone:     cf.Phone,
		INN:       cf.INN,
		CreatedAt: cf.CreatedAt.Unix(),
		UpdatedAt: nullTimeUnix(cf.UpdatedAt),
	}
}

var phoneRegexp = regexp.MustCompile(`\d{10,13}`)

func validatePhone(phone string) bool {
	return phoneRegexp.MatchString(phone)
}
