package admincafe

import (
	"context"
	"fmt"

	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/errreason"
	"gitlab.com/cafemenu/cafemenu/internal/hu"
	. "gitlab.com/cafemenu/cafemenu/internal/localization"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/models"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops/admin"
)

func validateCreateReq(req *models.AdminCafeCreateReq) string {
	if !validatePhone(req.Phone) {
		return L("phone is not valid")
	}

	return ""
}

func HandleCreateCafe(ctx context.Context, dbh *db.DB, p admin.CreateCafeParams, u interface{}) middleware.Responder {
	if msg := validateCreateReq(p.Req); msg != "" {
		return hu.ErrorResponder(p.HTTPRequest, errreason.UserErrorf(msg).WithReason(errreason.BadRequest))
	}

	cf, err := createCafe(ctx, dbh, p.Req)
	if err != nil {
		return hu.ErrorResponder(p.HTTPRequest, err)
	}

	return admin.NewCreateCafeOK().WithPayload(cf.toAPI())
}

func createCafe(ctx context.Context, dbh *db.DB, req *models.AdminCafeCreateReq) (cf *cafe, err error) {
	err = dbh.RunTx(ctx, func(tx *db.DB) error {
		cf, err = createCafeTx(ctx, tx, req)
		return err
	})

	return cf, err
}

func createCafeTx(ctx context.Context, tx *db.DB, req *models.AdminCafeCreateReq) (*cafe, error) {
	q := `select exists (select 1 from cafes where name = $1) x`
	var exists bool
	if err := tx.Get(ctx, &exists, q, req.Name); err != nil {
		return nil, err
	}

	if exists {
		return nil, errreason.UserErrorf(L("cafe with name '%s' already exists"), req.Name).WithReason(errreason.Conflict)
	}

	q = fmt.Sprintf(`insert into cafes (name, address, phone, inn) values
		($1, $2, $3, $4)
		returning %s`, cafeFields)
	var cf cafe
	if err := tx.Get(ctx, &cf, q, req.Name, req.Phone, req.Address, req.INN); err != nil {
		return nil, err
	}

	return &cf, nil
}
