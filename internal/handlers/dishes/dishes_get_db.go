package dishes

import (
	"context"

	"gitlab.com/cafemenu/cafemenu/internal/db"
)

type DishesDB struct {
	db *db.DB
}

func (d *DishesDB) GetDishes(ctx context.Context, cid string) (n int64, dishes []Dish, err error) {
	q := `/* get list of dishes for the given customer */
		SELECT id, name, description FROM dishes WHERE cafe_id = $1`

	if err = d.db.Select(ctx, &dishes, q, cid); err != nil {
		return 0, nil, err
	}

	q = `/* get number of dishes for the given customer */
	SELECT COUNT(*) FROM dishes WHERE cafe_id = $1`
	if err = d.db.Get(ctx, &n, q, cid); err != nil {
		return 0, nil, err
	}

	return n, dishes, nil
}
