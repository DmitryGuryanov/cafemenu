package dishes

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/hu"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/models"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops"
)

type DishesRest struct {
	store DishesDB
}

func New(db *db.DB) *DishesRest {
	return &DishesRest{
		store: DishesDB{
			db: db,
		},
	}
}

func (h *DishesRest) HandleGetDishes(p ops.GetDishesParams) middleware.Responder {
	n, dishes, err := h.store.GetDishes(p.HTTPRequest.Context(), p.CustomerID)
	if err != nil {
		return hu.ErrorResponder(p.HTTPRequest, err)
	}

	ret := &models.DishesList{
		Total: n,
	}

	for _, dish := range dishes {
		ret.Items = append(ret.Items, &models.Dish{
			ID:   dish.ID,
			Name: dish.Name,
		})
	}

	return ops.NewCreateDishOK().WithPayload(ret)
}
