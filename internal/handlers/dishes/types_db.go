package dishes

type Dish struct {
	ID          string `db:"id"`
	Name        string `db:"name"`
	Description string `db:"description"`
}
