package auth

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/cafemenu/cafemenu/internal/hu"
)

type contextKey int64

const (
	contextKeyUserID contextKey = iota
)

func (h *Auth) CookieAuth(ctx context.Context, token string) (context.Context, interface{}, error) {
	idVal := ctx.Value(contextKeyUserID)
	if idVal == nil {
		return ctx, nil, nil
	}

	id, ok := idVal.(int64)
	if !ok {
		return ctx, nil, fmt.Errorf("invalid user id type in context: %T", idVal)
	}

	return ctx, id, nil
}

func (h *Auth) NewAuthMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		cookie, err := r.Cookie(cookieName)
		switch err {
		case nil:
		case http.ErrNoCookie:
			// По этому пути идем если аутентификация не требуется
			handler.ServeHTTP(w, r)
			return
		default:
			hu.WriteError(w, r, err)
			return
		}

		id, err := h.validateToken(ctx, cookie.Value)
		if err != nil {
			hu.WriteError(w, r, err)
			return
		}

		ctx = context.WithValue(ctx, contextKeyUserID, id)
		handler.ServeHTTP(w, r.WithContext(ctx))
	})

}

func (h *Auth) validateToken(ctx context.Context, jwtToken string) (int64, error) {
	claims := &jwt.StandardClaims{}
	token, err := jwt.ParseWithClaims(jwtToken, claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodECDSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return &h.key.PublicKey, nil
	})

	if err != nil {
		return 0, fmt.Errorf("validate JWT token: %w", err)
	}

	if !token.Valid {
		return 0, fmt.Errorf("JWT token is not valid")
	}

	id, err := strconv.ParseInt(claims.Subject, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("invalid user ID: '%s', must be integer", claims.Subject)
	}

	return id, nil
}
