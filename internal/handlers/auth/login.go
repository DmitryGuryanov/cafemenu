package auth

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cafemenu/cafemenu/internal/hu"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops"
)

func (a *Auth) makeToken(id int) (string, error) {
	claims := jwt.StandardClaims{
		ExpiresAt: time.Now().Unix() + 86400,
		Subject:   strconv.Itoa(id),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES512, claims)
	ss, err := token.SignedString(a.key)
	if err != nil {
		panic(fmt.Sprintf("Can't sign: %v", err)) // FIXME: return error?
	}

	return ss, err
}

type CookieResponder struct {
	responder middleware.Responder
	cookies   []http.Cookie
}

func NewCookieResponder(responder middleware.Responder, cookies []http.Cookie) *CookieResponder {
	return &CookieResponder{
		responder: responder,
		cookies:   cookies,
	}
}

func makeAuthCookie(key, value string, maxAge int) http.Cookie {
	return http.Cookie{
		Name:     key,
		Value:    value,
		Secure:   false,
		SameSite: http.SameSiteDefaultMode,
		MaxAge:   maxAge,
		Path:     "/",
		HttpOnly: true,
	}
}

func (this *CookieResponder) WriteResponse(rw http.ResponseWriter, p runtime.Producer) {
	for i := range this.cookies {
		http.SetCookie(rw, &this.cookies[i])
	}
	this.responder.WriteResponse(rw, p)
}

func (h *Auth) HandlePostLogin(p ops.PostLoginParams) middleware.Responder {
	if p.Req.Login != "admin" || p.Req.Password != "qweqwe" {
		return hu.ErrorResponder(p.HTTPRequest, fmt.Errorf("login or password is invalid"))
	}

	token, err := h.makeToken(7)
	if err != nil {
		return hu.ErrorResponder(p.HTTPRequest, err)
	}

	cookie := makeAuthCookie(cookieName, token, 86400)

	resp := ops.PostLoginOKBody{
		Login: "admin",
		Name:  "Админ",
	}

	return NewCookieResponder(ops.NewPostLoginOK().WithPayload(&resp), []http.Cookie{cookie})
}
