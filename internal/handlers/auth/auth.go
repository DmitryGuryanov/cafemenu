package auth

import (
	"bytes"
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"database/sql"
	"encoding/pem"
	"errors"

	"gitlab.com/cafemenu/cafemenu/internal/db"
)

const cookieName = "jwt"

type Auth struct {
	db  *db.DB
	key *ecdsa.PrivateKey
}

func New(ctx context.Context, db *db.DB) (*Auth, error) {
	a := &Auth{
		db: db,
	}

	var keys struct {
		Public  string `db:"public"`
		Private string `db:"private"`
	}

	err := db.Get(ctx, &keys, `SELECT private, public FROM keys limit 1`)
	if errors.Is(err, sql.ErrNoRows) {
		a.key, err = ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
		if err != nil {
			return nil, err
		}

		x509EncodedPriv, err := x509.MarshalECPrivateKey(a.key)
		if err != nil {
			return nil, err
		}

		var privateKeyData bytes.Buffer
		if err = pem.Encode(&privateKeyData, &pem.Block{Type: "PRIVATE KEY", Bytes: x509EncodedPriv}); err != nil {
			return nil, err
		}

		x509EncodedPub, err := x509.MarshalPKIXPublicKey(a.key.Public())
		if err != nil {
			return nil, err
		}

		var publicKeyData bytes.Buffer
		if err = pem.Encode(&publicKeyData, &pem.Block{Type: "PUBLIC KEY", Bytes: x509EncodedPub}); err != nil {
			return nil, err
		}

		q := `INSERT INTO keys (private, public) VALUES ($1, $2)`
		if _, err := db.Exec(ctx, q, privateKeyData.String(), publicKeyData.String()); err != nil {
			return nil, err
		}
	} else if err == nil {
		block, _ := pem.Decode([]byte(keys.Private))
		x509Encoded := block.Bytes
		a.key, err = x509.ParseECPrivateKey(x509Encoded)
		if err != nil {
			return nil, err
		}
	} else if err != nil {
		return nil, err
	}
	return a, nil
}
