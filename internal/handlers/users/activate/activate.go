package activate

import (
	"context"

	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/cafemenu/cafemenu/internal/common"
	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/errreason"
	"gitlab.com/cafemenu/cafemenu/internal/hu"
	. "gitlab.com/cafemenu/cafemenu/internal/localization"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops"
)

func HandlePostRegisterActivate(dbh *db.DB, p ops.PostRegisterUserIDActivateParams) middleware.Responder {
	if err := activateUser(p.HTTPRequest.Context(), dbh, p.UserID, p.Req.Code); err != nil {
		return hu.ErrorResponder(p.HTTPRequest, err)
	}
	return ops.NewPostRegisterUserIDActivateOK()
}

func activateUser(ctx context.Context, dbh *db.DB, uid int64, userCode string) error {
	return dbh.RunTx(ctx, func(tx *db.DB) error {
		return activateUserTx(ctx, tx, uid, userCode)
	})
}

func activateUserTx(ctx context.Context, tx *db.DB, uid int64, userCode string) error {
	var dbCode string
	q := `SELECT activation_code FROM users WHERE id = $1`
	if err := tx.Get(ctx, &dbCode, q, uid); err != nil {
		return err
	}

	if dbCode != userCode {
		return errreason.Errorf(L("activation code is invalid (internal)")).
			WithMsg(L("activation code is invalid")).
			WithReason(errreason.BadRequest)
	}

	q = `UPDATE users SET state = $1 WHERE id = $2`
	if _, err := tx.Exec(ctx, q, common.UserStateActive, uid); err != nil {
		return err
	}

	return nil
}
