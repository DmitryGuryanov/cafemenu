package register

import (
	"context"
	"crypto/rand"
	"fmt"
	"math"
	"math/big"
	"regexp"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
	"gitlab.com/cafemenu/cafemenu/internal/common"
	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/errreason"
	"gitlab.com/cafemenu/cafemenu/internal/hu"
	. "gitlab.com/cafemenu/cafemenu/internal/localization"
	"gitlab.com/cafemenu/cafemenu/internal/notify"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops"
)

type Handler struct {
	db     *db.DB
	notify *notify.Notifier
}

func New(dbh *db.DB, n *notify.Notifier) *Handler {
	return &Handler{
		db:     dbh,
		notify: n,
	}
}

var emailRe = regexp.MustCompile(`(?:[a-z0-9!#$%&'*+/=?^_` + "`" + `{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_` + "`" + `{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])`)

func makeCode() string {
	x, err := rand.Int(rand.Reader, big.NewInt(1<<62))
	if err != nil {
		panic(fmt.Sprintf("rand error: %+v", err))
	}

	return fmt.Sprintf("%.4d", x.Int64()%(int64(math.Pow10(4))))
}

func validateRegisterReq(req ops.PostRegisterBody) error {
	if !emailRe.MatchString(swag.StringValue(req.Email)) {
		return errreason.UserErrorf(L("email is invalid: %s"), swag.StringValue(req.Email)).
			WithReason(errreason.BadRequest)
	}

	return nil
}

func (h *Handler) HandlePostRegister(p ops.PostRegisterParams) middleware.Responder {
	if err := validateRegisterReq(p.Req); err != nil {
		return hu.ErrorResponder(p.HTTPRequest, err)
	}

	uid, err := h.createUser(p.HTTPRequest.Context(), p.Req)
	if err != nil {
		return hu.ErrorResponder(p.HTTPRequest, err)
	}

	resp := ops.PostRegisterOKBody{
		ID: uid,
	}

	return ops.NewPostRegisterOK().WithPayload(&resp)
}

func (h *Handler) createUser(ctx context.Context, req ops.PostRegisterBody) (uid int64, err error) {
	err = h.db.RunTx(ctx, func(tx *db.DB) error {
		uid, err = h.createUserTx(ctx, tx, req)
		return err
	})

	return uid, err
}

func (h *Handler) createUserTx(ctx context.Context, tx *db.DB, req ops.PostRegisterBody) (int64, error) {
	q := `SELECT EXISTS(SELECT FROM users WHERE email = $1)`
	var exists bool
	if err := tx.Get(ctx, &exists, q, req.Email); err != nil {
		return 0, err
	}

	if exists {
		return 0, errreason.UserErrorf(L("user with email %s already exists"), *req.Email).
			WithReason(errreason.Conflict)
	}

	args := map[string]interface{}{
		"email":           req.Email,
		"display_name":    req.DisplayName,
		"password":        common.MakePasswdHash(swag.StringValue(req.Password)),
		"state":           common.UserStateWaitActivation,
		"activation_code": makeCode(),
	}
	q = `INSERT INTO users (email, display_name, password, state, activation_code, created_at) VALUES
		(:email, :display_name, :password, :state, :activation_code, now())
		RETURNING id`

	var id int64
	if err := h.db.NamedGet(ctx, &id, q, args); err != nil {
		return 0, err
	}

	return id, nil
}
