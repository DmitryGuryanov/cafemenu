package common

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"strings"

	"golang.org/x/crypto/pbkdf2"
)

type UserState string

const (
	UserStateWaitActivation UserState = "wait_activation"
	UserStateActive         UserState = "active"
	UserStateInactive       UserState = "inactive"
)

type User struct {
	ID          int64
	Email       string
	DisplayName string
	State       UserState
}

func MakePasswdHash(pw string) string {
	salt := make([]byte, 8)
	n, err := rand.Read(salt)
	if err != nil || n != len(salt) {
		panic("failed to generate random bytes")
	}
	salt = []byte(hex.EncodeToString(salt))

	return passwdHash(pw, salt)
}

func passwdHash(pw string, salt []byte) string {
	dk := pbkdf2.Key([]byte(pw), salt, 10000, 32, sha256.New)
	return string(salt) + ":" + hex.EncodeToString(dk)
}

func IsPasswordValid(hash, pw string) bool {
	if hash == "" {
		return false
	}

	hashItems := strings.SplitN(hash, ":", 2)

	hash2 := passwdHash(pw, []byte(hashItems[0]))
	return hash == hash2
}
