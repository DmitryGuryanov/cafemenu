package localization

var ru = map[string]string{
	"activation code is invalid":        "неверный код активации",
	"user with email %s already exists": "пользователь с email %s уже существует",
	"email is invalid: %s":              "email '%s' некорректный",
}

func L(s string) string {
	if ls, ok := ru[s]; ok {
		return ls
	}
	return s
}
