create table if not exists keys (
    private text not null,
    public text not null
);

create type user_state as enum ('wait_activation', 'active', 'inactive', 'deleted');
create table if not exists users (
    id bigserial primary key,
    email text unique not null,
    password text,
    display_name text not null,
    state user_state not null,
    activation_code text,
    created_at timestamptz,
    activated_at timestamptz
);

create table if not exists cafes (
	id bigserial primary key,
	name text not null unique,
	address text not null,
	phone text not null,
	inn text,
	created_at timestamptz not null default now(),
	updated_at timestamptz,
	deleted_at timestamptz
);
