gen:
	swagger generate server -C ./api/configs/config.yaml --template-dir ./api/configs/templates --exclude-main -s internal/restapi  -m internal/restapi/models  -a ops  -f ./api/swagger.yaml

gen-tsx:
	openapi-generator generate -g typescript-fetch -i ./api/swagger.yaml -o ../cafemenu-front/src/apigen
