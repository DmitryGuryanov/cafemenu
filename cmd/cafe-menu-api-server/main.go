package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/loads"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"github.com/gorilla/handlers"
	flags "github.com/jessevdk/go-flags"
	"gitlab.com/cafemenu/cafemenu/internal/db"
	"gitlab.com/cafemenu/cafemenu/internal/handlers/admincafe"
	"gitlab.com/cafemenu/cafemenu/internal/handlers/auth"
	"gitlab.com/cafemenu/cafemenu/internal/handlers/dishes"
	"gitlab.com/cafemenu/cafemenu/internal/handlers/users/activate"
	"gitlab.com/cafemenu/cafemenu/internal/handlers/users/register"
	"gitlab.com/cafemenu/cafemenu/internal/logger"
	"gitlab.com/cafemenu/cafemenu/internal/notify"
	"gitlab.com/cafemenu/cafemenu/internal/restapi"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops"
	"gitlab.com/cafemenu/cafemenu/internal/restapi/ops/admin"
)

func configureAPI(api *ops.CafeMenuAPIAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf
	api.Logger = logger.L().Infof

	// api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()
	api.JSONProducer = runtime.JSONProducer()

	if api.GetDishesHandler == nil {
		api.GetDishesHandler = ops.GetDishesHandlerFunc(func(params ops.GetDishesParams) middleware.Responder {
			return middleware.NotImplemented("operation ops.GetDishes has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

func setupGlobalMiddleware(handler http.Handler) http.Handler {
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Origin", "Options", "Accept", "Content-Type", "Cache-Control", "Content-Language", "Expires", "Last-Modified", "Pragma"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"})
	exposedHeaders := handlers.ExposedHeaders([]string{"SetCookie"})

	return handlers.CORS(originsOk, headersOk, methodsOk, exposedHeaders)(handler)
}

func main() {
	if err := logger.Init(); err != nil {
		panic(fmt.Sprintf("logger initialization error: %+v", err))
	}

	swaggerSpec, err := loads.Embedded(restapi.SwaggerJSON, restapi.FlatSwaggerJSON)
	if err != nil {
		log.Fatalln(err)
	}

	api := ops.NewCafeMenuAPIAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer func() {
		if err := server.Shutdown(); err != nil {
			logger.L().Infof("server shutdown error: %+v", err)
		}
	}()

	parser := flags.NewParser(server, flags.Default)
	parser.ShortDescription = "CafeMenu API"
	parser.LongDescription = "CafeMenu API"
	server.ConfigureFlags()
	for _, optsGroup := range api.CommandLineOptionsGroups {
		_, err := parser.AddGroup(optsGroup.ShortDescription, optsGroup.LongDescription, optsGroup.Options)
		if err != nil {
			log.Fatalln(err)
		}
	}

	if _, err := parser.Parse(); err != nil {
		code := 1
		if fe, ok := err.(*flags.Error); ok {
			if fe.Type == flags.ErrHelp {
				code = 0
			}
		}
		os.Exit(code)
	}

	db, err := db.Init(os.Getenv("DB"))
	if err != nil {
		log.Fatalln(err)
	}

	notifier := notify.New(db)

	ctx := context.Background()
	auth, err := auth.New(ctx, db)
	if err != nil {
		log.Fatalln(err)
	}
	api.CookieAuthAuth = auth.CookieAuth

	api.PostLoginHandler = ops.PostLoginHandlerFunc(auth.HandlePostLogin)

	registerHandler := register.New(db, notifier)
	api.PostRegisterHandler = ops.PostRegisterHandlerFunc(registerHandler.HandlePostRegister)

	api.PostRegisterUserIDActivateHandler = ops.PostRegisterUserIDActivateHandlerFunc(
		func(p ops.PostRegisterUserIDActivateParams) middleware.Responder {
			return activate.HandlePostRegisterActivate(db, p)
		})

	dishes := dishes.New(db)
	api.GetDishesHandler = ops.GetDishesHandlerFunc(dishes.HandleGetDishes)

	api.AdminCreateCafeHandler = admin.CreateCafeHandlerFunc(
		func(p admin.CreateCafeParams, u interface{}) middleware.Responder {
			return admincafe.HandleCreateCafe(p.HTTPRequest.Context(), db, p, u)
		})

	api.AdminGetCafeListHandler = admin.GetCafeListHandlerFunc(
		func(p admin.GetCafeListParams, u interface{}) middleware.Responder {
			return admincafe.HandleGetCafeList(p.HTTPRequest.Context(), db, p, u)
		})

	api.AdminGetCafeHandler = admin.GetCafeHandlerFunc(
		func(p admin.GetCafeParams, u interface{}) middleware.Responder {
			return admincafe.HandleGetCafe(p.HTTPRequest.Context(), db, p, u)
		})

	api.AdminUpdateCafeHandler = admin.UpdateCafeHandlerFunc(
		func(p admin.UpdateCafeParams, u interface{}) middleware.Responder {
			return admincafe.HandleUpdateCafe(p.HTTPRequest.Context(), db, p, u)
		})

	api.AdminDeleteCafeHandler = admin.DeleteCafeHandlerFunc(
		func(p admin.DeleteCafeParams, u interface{}) middleware.Responder {
			return admincafe.HandleDeleteCafe(p.HTTPRequest.Context(), db, p, u)
		})

	// server.ConfigureAPI()
	configureAPI(api)
	server.SetHandler(setupGlobalMiddleware(api.Serve(auth.NewAuthMiddleware)))

	if err := server.Serve(); err != nil {
		log.Fatalln(err)
	}

}
